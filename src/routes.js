import * as React from "react";
import RRouter from "_m/utils/react-router";
import QuestionListConnector from "_m/question/QuestionListConnector";
import NotFound from "_m/ui/MtNotFound";
import TestAsync from "./TestAsync";

// import { AuthRoute } from "@abb/controller";

const { Switch, Route } = RRouter;

export const routes = {
  home: {
    title: `Home`,
    path: `/`,
    component: TestAsync
  },
  questions: {
    title: `Questions`,
    path: `/questions`,
    component: QuestionListConnector
  },
  notFound: {
    component: NotFound
  }
};

const Routes = () => (
  <Switch>
    <Route exact {...routes.home} />
    <Route exact {...routes.questions} />
    <Route {...routes.notFound} />
  </Switch>
);
Routes.displayName = `Routes`;
export default Routes;
