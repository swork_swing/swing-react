import React from "react";
import { StyleSheet, Text, View } from "react-native";
import RRouter from "_m/utils/react-router";
import { routes } from "_s/Routes";

const { Router, Link } = RRouter;

const timeout = ms => new Promise(resolve => setTimeout(resolve, ms));

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: `#fff`,
    alignItems: `center`,
    justifyContent: `center`
  }
});

export default class TestAsync extends React.Component {
  constructor(props) {
    super(props);

    this.state = { txt: `click me` };
    this.sleep = this.sleep.bind(this);
  }

  async sleep() {
    this.setState({ txt: `waiting` });
    await timeout(1000);
    this.setState({ txt: `done` });
  }

  render() {
    const { txt } = this.state;
    return (
      <View style={styles.container}>
        <Text onPress={this.sleep}>{txt}</Text>
        <Text>{Router.prototype.constructor.name}</Text>
        <Link to={routes.questions.path}>
          <Text>Go to Question</Text>
        </Link>
      </View>
    );
  }
}
