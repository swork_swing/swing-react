import { ApolloClient } from "apollo-client";
import { createHttpLink } from "apollo-link-http";
import { InMemoryCache } from "apollo-cache-inmemory";
import { REACT_APP_SERVER_URL } from "_s/config";

export default new ApolloClient({
  link: createHttpLink({
    uri: REACT_APP_SERVER_URL
  }),
  cache: new InMemoryCache()
});
