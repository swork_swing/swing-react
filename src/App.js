import React from "react";
import Routes from "_s/Routes";
import { ApolloProvider } from "react-apollo";
import Layout from "_m/ui/MtLayout";
import RRouter from "_m/utils/react-router";
import apolloClient from "./apollo";

const { Router } = RRouter;

export default () => (
  <ApolloProvider client={apolloClient}>
    <Router>
      <Layout>
        <Routes />
      </Layout>
    </Router>
  </ApolloProvider>
);
