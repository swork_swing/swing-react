import React from "react";
import { StyleSheet, Text } from "react-native";
import MtList from "_m/ui/MtList";

const styles = StyleSheet.create({
  row: {}
});

// updatedAt "2018-08-08T22:01:21.838Z"
const QuestionItem = ({ id, title, author, updatedAt }) => (
  <MtList.Row key={id} style={styles.row}>
    <Text>{title}</Text>
    {!!author && author.name && <Text>{author.name}</Text>}
    <Text>updated at {updatedAt}</Text>
  </MtList.Row>
);

export default QuestionItem;
