import * as React from "react";

import MtList from "_m/ui/MtList";
import { View, Text } from "react-native";
import RRouter from "_m/utils/react-router";
import { routes } from "_s/Routes";
import QuestionItem from "./ui/QuestionItem";

const { Link } = RRouter;

class QuestionListConnector extends React.Component {
  constructor(props) {
    super(props);
    this.state = this.getInitialState();
  }

  getInitialState() {
    return {};
  }

  componentDidMount() {
    // const { _questionList } = this.props;
    // this.setState(this.getInitialState(), () => _questionList());
  }

  render() {
    // const { questions } = this.props;
    const questions = [
      {
        id: `1`,
        title: `Prisma turns your database into a GraphQL API 😎 😎`,
        author: {
          name: `blemoine`
        }
      },
      {
        id: `2`,
        title: `The best GraphQL client`,
        author: {
          name: `blemoine`
        }
      }
    ];
    return (
      <View>
        <MtList ItemComponent={QuestionItem} items={questions} />
        <Link to={routes.home.path}>
          <Text>Back to Home</Text>
        </Link>
      </View>
    );
  }
}

export default QuestionListConnector;
