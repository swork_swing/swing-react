import * as RRDom from "react-router-dom";

export default {
  ...RRDom,
  Router: RRDom.BrowserRouter
};
