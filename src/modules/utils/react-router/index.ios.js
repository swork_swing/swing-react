import * as RRNative from "react-router-native";

export default {
  ...RRNative,
  Router: RRNative.NativeRouter
};
