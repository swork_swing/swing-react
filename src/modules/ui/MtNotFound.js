import React from "react";
import { View, Text } from "react-native";

import RRouter from "_m/utils/react-router";
import { routes } from "_s/Routes";

const { Link } = RRouter;

const NotFound = () => (
  <View>
    <Text>404 Page not found</Text>

    <Link to={routes.home.path}>Back to Home</Link>
  </View>
);

export default NotFound;
