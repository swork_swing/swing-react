const theme = {
  primaryColor: `#16669b`,
  onPrimaryColor: `#FFFFFF`,

  primaryColorLight: `#5593cc`,
  onPrimaryColorLight: `#000000`,

  primaryColorDark: `003c6c`,
  onPrimaryColorDark: `#FFFFFF`,

  // Floating action buttons
  // Selection controls, like sliders and switches
  // Highlighting selected text
  // Progress bars
  // Links and headlines
  secondaryColor: `67BC0A`,
  onSecondaryColor: `#FFFFFF`,

  // Behind scrollable content.
  backgroundColor: `#FFFFFF`,
  onBackgroundColor: `#000000`,

  // Background of components like cards, sheets, and menus.
  surfaceColor: `#FFFFFF`,
  onSurfaceColor: `#000000`,

  // indicates errors components, such as text fields.
  errorColor: `#B00020`,
  onErrorColor: `#FFFFFF`
};

export default theme;

// color tool https://material.io/tools/color/#!/?view.left=0&view.right=1&primary.color=0277BD
// bleus de la maquette ...
// 16669b : menu profil
// 1C6CA1 : topbar
// 3482B5 : logo
// 1C6CA1 : lien txt
// 0F5D90 : txt tab
// 9B9B9B * 8% (alpha) : bg comment
