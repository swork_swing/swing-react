// import React from "react";
// import { TouchableOpacity, StyleSheet, Text } from "react-native";

// import { Link } from "_m/utils/react-router";

// const LinkButton = ({
//   title,

//   accessibilityComponentType,
//   accessibilityLabel,
//   accessibilityRole,
//   accessibilityTraits,
//   accessible,
//   activeOpacity,
//   delayLongPress,
//   delayPressIn,
//   delayPressOut,
//   disabled,
//   focusedOpacity,
//   hitSlop,
//   onLayout,
//   onLongPress,
//   onPress,
//   onPressIn,
//   onPressOut,
//   pressRetentionOffset,
//   testID,

//   numberOfLines,
//   selectable,

//   to,
//   replace
// }) => (
//   <Link to={to} replace={replace}>
//     <TouchableOpacity
//       accessibilityComponentType={accessibilityComponentType}
//       accessibilityLabel={accessibilityLabel}
//       accessibilityRole={accessibilityRole}
//       accessibilityTraits={accessibilityTraits}
//       accessible={accessible}
//       activeOpacity={activeOpacity}
//       delayLongPress={delayLongPress}
//       delayPressIn={delayPressIn}
//       delayPressOut={delayPressOut}
//       disabled={disabled}
//       focusedOpacity={focusedOpacity}
//       hitSlop={hitSlop}
//       onLayout={onLayout}
//       onLongPress={onLongPress}
//       onPress={onPress}
//       onPressIn={onPressIn}
//       onPressOut={onPressOut}
//       pressRetentionOffset={pressRetentionOffset}
//       testID={testID}
//     >
//       <Text numberOfLines={numberOfLines} selectable={selectable}>
//         {title}
//       </Text>
//     </TouchableOpacity>
//   </Link>
// );

// export default LinkButton;

// StyleSheet.create({
//   container: {
//     flexDirection: `row`,
//     justifyContent: `space-between`,
//     paddingLeft: 10,
//     paddingRight: 10,
//     marginBottom: 10,
//     alignItems: `center`,
//     backgroundColor: `#C5CCCF`,
//     color: `white`,
//     height: 35,
//     width: `100%`
//   }
// });

import React from "react";
import { TouchableOpacity, StyleSheet, Text } from "react-native";

import RRouter from "_m/utils/react-router";

const { Link } = RRouter;

const LinkButton = ({ title, numberOfLines, selectable, to, replace }) => (
  <Link to={to} replace={replace}>
    <Text numberOfLines={numberOfLines} selectable={selectable}>
      {title}
    </Text>
  </Link>
);

export default LinkButton;
