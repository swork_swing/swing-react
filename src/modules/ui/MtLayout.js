import React from "react";
import { View, StyleSheet } from "react-native";

import NavBar from "_m/ui/MtNavBar";
import { routes } from "_s/Routes";

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: `flex-start`,
    alignItems: `center`,
    backgroundColor: `#F5FCFF`
    // borderColor: 'green',
    // borderWidth: 0.5,
  },
  content: {
    justifyContent: `flex-start`,
    alignItems: `center`
    // borderColor: 'blue',
    // borderWidth: 0.5,
  }
});

const navRoutes = Object.entries(routes)
  .filter(route => !!route[1].title)
  .map(route => ({
    key: route[0],
    to: route[1].path,
    title: route[1].title
  }));

const Layout = ({ children }) => (
  <View style={styles.container}>
    <NavBar routes={navRoutes} />
    <View style={styles.content}>{children}</View>
  </View>
);
Layout.displayName = `Layout`;
export default Layout;
