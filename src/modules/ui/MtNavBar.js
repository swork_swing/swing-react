import React from "react";
import { View, StyleSheet } from "react-native";
import LinkButton from "_m/ui/MtLinkButton";

const styles = StyleSheet.create({
  container: {
    flexDirection: `row`,
    justifyContent: `space-between`,
    paddingLeft: 10,
    paddingRight: 10,
    marginBottom: 10,
    alignItems: `center`,
    backgroundColor: `#C5CCCF`,
    height: 35,
    width: `100%`
  }
});

const NavBar = ({ routes }) => (
  <View style={styles.container}>
    {routes.map(r => (
      <LinkButton {...r} key={r.key} />
    ))}
  </View>
);

export default NavBar;
