import React from "react";
import { StyleSheet, FlatList, View } from "react-native";

const styles = StyleSheet.create({
  list: {},
  row: {
    flex: 1,
    flexDirection: `row`
  }
});

class MtList extends React.Component {
  static Row(props) {
    const { children } = props;
    return (
      <View style={styles.row} {...props}>
        {children}
      </View>
    );
  }

  render() {
    const { ItemComponent, items } = this.props;
    return (
      <FlatList
        style={styles.list}
        data={items}
        renderItem={({ item }) => <ItemComponent {...item} />}
      />
    );
  }
}

export default MtList;
