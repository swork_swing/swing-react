var extensions = [".js", ".ios.js", ".android.js", ".native.js", ".web.js"];

module.exports = {
  env: {
    browser: true,
    commonjs: true,
    es6: true,
    node: true
  },
  extends: ["airbnb", "prettier", "prettier/react"],
  parser: "babel-eslint",
  parserOptions: {
    ecmaFeatures: {
      jsx: true
    },
    ecmaVersion: 2018,
    sourceType: "module"
  },
  plugins: ["prettier"],
  rules: {
    "linebreak-style": ["error", "unix"],
    "import/prefer-default-export": ["warn"],
    "no-plusplus": ["off"],
    "prettier/prettier": ["error"],
    "react/prop-types": ["off"], // Handled with flow
    "react/no-unused-prop-types": ["off"],
    "react/prefer-stateless-function": ["warn"],
    "react/jsx-filename-extension": ["off"],
    quotes: ["error", "backtick"]
  },
  settings: {
    "import/ignore": ["node_modules", "\\.json$"],
    "import/resolve": {
      extensions: extensions
    },
    "import/resolver": {
      node: {
        extensions: extensions
      },
      webpack: {
        config: "webpack.config.js"
      },
      "babel-module": {}
    }
  }
};
